<?php

/**
 * Created by PhpStorm.
 * User: Dell XPS13
 * Date: 2017-11-10
 * Time: 19:41
 */
require_once 'dbConfig.php';
class User{}

    function checkUser($userData = array()){
        if(!empty($userData)){
            //Check whether user data already exists in database
            $db = connectToDB();
            $prevQuery = "SELECT * FROM users WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
            $prevResult = $db->query($prevQuery);
            if($prevResult->num_rows > 0){
                //Update user data if already exists
                $query = "UPDATE users SET first_name = '".$userData['first_name']."', last_name = '".$userData['last_name']."', email = '".$userData['email']."', gender = '".$userData['gender']."', locale = '".$userData['locale']."', link = '".$userData['link']."', modified = '".date("Y-m-d H:i:s")."' WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
                $update = $db->query($query);
            }else{
                //Insert user data
                $query = "INSERT INTO users SET oauth_provider = '".$userData['oauth_provider']."', oauth_uid = '".$userData['oauth_uid']."', first_name = '".$userData['first_name']."', last_name = '".$userData['last_name']."', email = '".$userData['email']."', gender = '".$userData['gender']."', locale = '".$userData['locale']."', picture = '".$userData['picture']."', link = '".$userData['link']."', created = '".date("Y-m-d H:i:s")."', modified = '".date("Y-m-d H:i:s")."'";
                $insert = $db->query($query);
            }

            //Get user data from the database
            $result = $db->query($prevQuery);
            $userData = $result->fetch_assoc();
        }
        //Return user data
        return $userData;

}
?>