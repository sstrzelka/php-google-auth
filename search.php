<?php
require_once 'dbConfig.php';
//get search term
$searchTerm = $_GET['term'];
//get matched data from skills table
$db = connectToDB();
$query = $db->query("SELECT * FROM skills WHERE skill LIKE '%".$searchTerm."%' ORDER BY skill ASC");
while ($row = $query->fetch_assoc()) {
    $data[] = $row['skill'];
}
//return json data
echo json_encode($data);
?>