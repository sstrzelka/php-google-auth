<?php
require_once 'dbConfig.php';
error_reporting(E_ALL);
ini_set('display_errors','On');
if (isset($_GET['skills'])) {
    session_start();
    $userId = $_SESSION['userData']['id'];
    $db = connectToDB();
    $result = $db->query("UPDATE users SET skills = '" . $_GET['skills'] . "' WHERE id = $userId");
    header("Location:index.php");
    die();
}
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#skills" ).autocomplete({
            source: 'search.php'
        });
    });
</script>
</head>
<body>
<div class="ui-widget">
    <form action="" method="get">
        <label for="skills">Skills: </label>
        <input id="skills" name="skills">
        <input type="submit" value="Save">
    </form>
    <br/><a href="index.php">Main page</a>
</div>
</body>
</html>

